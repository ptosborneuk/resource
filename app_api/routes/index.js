var express = require('express');
var router = express.Router();
var ctrlHops = require('../controllers/hops');
var ctrlMalts = require('../controllers/malts');
var ctrlStyles = require('../controllers/styles');
var ctrlSugars = require('../controllers/sugars');
var ctrlYeasts = require('../controllers/yeasts');

// Stateless endpoints for RESTFUL API's

// hops - CRUD operations for the hops using HTML methods
router.post('/hops', ctrlHops.createHop);
router.get('/hops', ctrlHops.hops);
router.get('/hop/:hopid', ctrlHops.hopByID);
router.put('/hop/:hopid', ctrlHops.updateHop);
router.delete('/hop/:hopid', ctrlHops.deleteHop);

// Malts - CRUD operations for the malts using HTML methods
router.post('/malts', ctrlMalts.createMalt);
router.get('/malts', ctrlMalts.malts);
router.get('/malt/:maltid', ctrlMalts.maltByID);
router.put('/malt/:maltid', ctrlMalts.updateMalt);
router.delete('/malt/:maltid', ctrlMalts.deleteMalt);

// Styles - CRUD operations for the beer styles using HTML methods
router.post('/styles', ctrlStyles.createStyle);
router.get('/styles', ctrlStyles.styles);
router.get('/style/:styleid', ctrlStyles.styleByID);
router.put('/style/:styleid', ctrlStyles.updateStyle);
router.delete('/style/:styleid', ctrlStyles.deleteStyle);

// Sugars - CRUD operations for the brewing sugras using HTML methods
router.post('/sugars', ctrlSugars.createSugar);
router.get('/sugars', ctrlSugars.sugars);
router.get('/sugar/:sugarid', ctrlSugars.sugarByID);
router.put('/sugar/:sugarid', ctrlSugars.updateSugar);
router.delete('/sugar/:sugarid', ctrlSugars.deleteSugar);

// yeasts - CRUD operations for the yeasts using HTML methods
router.post('/yeasts', ctrlYeasts.createYeast);
router.get('/yeasts/:strainid', ctrlYeasts.yeasts);
router.get('/yeast/strains', ctrlYeasts.yeastStrains);
router.get('/yeast/:yeastid', ctrlYeasts.yeastByID);
router.put('/yeast/:yeastid', ctrlYeasts.updateYeast);
router.delete('/yeast/:yeastid', ctrlYeasts.deleteYeast);


// Export the router for the RESTFUL API
module.exports = router;
