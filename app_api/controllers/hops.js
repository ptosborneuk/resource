/*
  name: String
  alpha: String
  style: String
  substitute: String
  flavour: String
*/

var mongoose = require('mongoose');
var Hop = mongoose.model('hop');

// Create a new Hop item in the database //
module.exports.createHop = (req, res) => {
  Hop.create({
    name: req.body.name,
    alpha: req.body.alpha,
    style: req.body.style,
    substitute: req.body.substitute,
    flavour: req.body.flavour,
  },function(err, hop){
    if(err, hop){
      sendJSONResponse(res, 400, err);
    }else{
      sendJSONResponse(res, 200, hop);
    }
  });
};

// Get the beer style for all of the beer styles in the database //
module.exports.hops = (req, res) => {
  Hop
    .find()
    // .select('name')
    .exec(function(err, hop){
      sendJSONResponse(res, 200, hop);
    })
};

// Get all of the beer style data by the style_ID number //
module.exports.hopByID = (req, res) => {
  Hop
    .findById(req.params.hopid)
    .exec(function(err, hop){
      sendJSONResponse(res, 200, hop);
    })
};

// Update the Hop information by the ID number //
module.exports.updateHop = (req, res) => {};


// Delete the Hop from the database by the ID nmber //
module.exports.deleteHop = (req, res) => {
  var hopid = req.params.hopid;
  if(hopid){
    Hop
      .findByIdAndRemove(hopid)
      .exec(function(err, hop){
        if(err){
          sendJSONResponse(res, 404, err);
          return;
        }
        sendJSONResponse(res, 204, null);
      })
  } else {
    sendJSONResponse(res, 404,{
      "message": "No Hop ID found"
    });
  }
};

var sendJSONResponse = (res, status, content) => {
  res.status(status);
  res.json(content);
};
