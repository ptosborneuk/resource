/*
  name: String
  type: String
  lovibond: String
  srm: String
  ebc: String
  potential: String
  description: String
*/

var mongoose = require('mongoose');
var Malt = mongoose.model('malt');

// Create a new malt item in the database //
module.exports.createMalt = (req, res) => {
  Malt.create({
    name: req.body.name,
    type: req.body.type,
    lovibond: req.body.lovibond,
    srm: req.body.srm,
    ebc: req.body.ebc,
    potential: req.body.potential,
    description: req.body.description
  },function(err, malt ){
    if(err, malt){
      sendJSONResponse(res, 400, err);
    }else{
      sendJSONResponse(res, 200, malt);
    }
  });
};

// Get the name and description of all of the malts from the database //
module.exports.malts = (req, res) => {
  Malt
    .find()
  //  .select('name description')
    .exec(function(err, malt){
      sendJSONResponse(res, 200, malt);
    })
};

// Get malt from data by by the ID number //
module.exports.maltByID = (req, res) => {
  Malt
    .findById(req.params.maltid)
    .exec(function(err, malt){
      sendJSONResponse(res, 200, malt);
    })
};

// Update the malt information bythe ID number //
module.exports.updateMalt = (req, res) => {};


// Delete the Malt from the database by the ID nmber //
module.exports.deleteMalt = (req, res) => {
  var maltid = req.params.maltid;
  if(maltid){
    Malt
      .findByIdAndRemove(maltid)
      .exec(function(err, malt){
        if(err){
          sendJSONResponse(res, 404, err);
          return;
        }
        sendJSONResponse(res, 204, null);
      })
  } else {
    sendJSONResponse(res, 404,{
      "message": "No Malt ID found"
    });
  }
};

var sendJSONResponse = (res, status, content) => {
  res.status(status);
  res.json(content);
};
