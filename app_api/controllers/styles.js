/*
  style: String
  ibu: String
  srm: String
  original: String
  final: String
  abv: String
  carbination: String
  impression: String
  aroma: String
  appearance: String
  flavour: String
  mouthfeel: String
  comments: String
  history: String
  ingredients: String
  comparison: String
*/

var mongoose = require('mongoose');
var Style = mongoose.model('style');

// Create a new Beer Style item in the database //
module.exports.createStyle = (req, res) => {
  Style.create({
    style: req.body.style,
    ibu: req.body.ibu,
    srm: req.body.srm,
    original: req.body.original,
    final: req.body.final,
    abv: req.body.abv,
    carbination: req.body.carbination,
    impression: req.body.impression,
    aroma: req.body.aroma,
    appearance: req.body.appearance,
    flavour: req.body.flavour,
    mouthfeel: req.body.mouthfeel,
    comments: req.body.comments,
    history: req.body.history,
    ingredients: req.body.ingredients,
    comparison: req.body.comparison
  },function(err, style){
    if(err, style){
      sendJSONResponse(res, 400, err);
    }else{
      sendJSONResponse(res, 200, style);
    }
  });
};

// Get the beer style for all of the beer styles in the database //
module.exports.styles = (req, res) => {
  Style
    .find()
    .select('style comparison')
    .exec(function(err, style){
      sendJSONResponse(res, 200, style);
    })
};

// Get all of the beer style data by the style_ID number //
module.exports.styleByID = (req, res) => {
  Style
    .findById(req.params.styleid)
    .exec(function(err, style){
      sendJSONResponse(res, 200, style);
    })
};

// Update the Style information by the ID number //
module.exports.updateStyle = (req, res) => {};


// Delete the Style from the database by the ID nmber //
module.exports.deleteStyle = (req, res) => {
  var styleid = req.params.styleid;
  if(styleid){
    Style
      .findByIdAndRemove(styleid)
      .exec(function(err, style){
        if(err){
          sendJSONResponse(res, 404, err);
          return;
        }
        sendJSONResponse(res, 204, null);
      })
  } else {
    sendJSONResponse(res, 404,{
      "message": "No Style ID found"
    });
  }
};

var sendJSONResponse = (res, status, content) => {
  res.status(status);
  res.json(content);
};
