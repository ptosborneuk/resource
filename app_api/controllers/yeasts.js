/*
  code: String
  strain: String
  make: String
  brewery: String
*/


var mongoose = require('mongoose');
var Yeast = mongoose.model('yeast');

// Create a new Hop item in the database //
module.exports.createYeast = (req, res) => {
  Yeast.create({
    code: req.body.code,
    strain: req.body.strain,
    make: req.body.make,
    brewery: req.body.brewery
  },function(err, yeast){
    if(err, yeast){
      sendJSONResponse(res, 400, err);
    }else{
      sendJSONResponse(res, 200, yeast);
    }
  });
};

// Get the yeast code for all of the yeasts in the database //
module.exports.yeasts = (req, res) => {
  Yeast
    .find( {make:req.params.strainid})
    // .select('code')
    .exec(function(err, code){
      sendJSONResponse(res, 200, code);
    })
};

// Get the yeast strains from database - return only unique values //
module.exports.yeastStrains = (req, res) => {
  Yeast
    .find()
    .distinct('make')
    .exec(function(err, code){
      sendJSONResponse(res, 200, code);
    })
};

// Get all of the yeast data by the yeast_ID number //
module.exports.yeastByID = (req, res) => {
  Yeast
    .findById(req.params.yeastid)
    .exec(function(err, yeast){
      sendJSONResponse(res, 200, yeast);
    })
};

// Update the Sugar information by the ID number //
module.exports.updateYeast = (req, res) => {};


// Delete the Sugar from the database by the ID nmber //
module.exports.deleteYeast = (req, res) => {
  var yeastid = req.params.yeastid;
  if(yeastid){
    Yeast
      .findByIdAndRemove(yeastid)
      .exec(function(err, yeast){
        if(err){
          sendJSONResponse(res, 404, err);
          return;
        }
        sendJSONResponse(res, 204, null);
      })
  } else {
    sendJSONResponse(res, 404,{
      "message": "No Yeast ID found"
    });
  }
};

var sendJSONResponse = (res, status, content) => {
  res.status(status);
  res.json(content);
};
