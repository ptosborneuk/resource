/*
  sugar: String
  lovibond: String
  potential: String
  description: String
*/

var mongoose = require('mongoose');
var Sugar = mongoose.model('sugar');


// Create a new Sugar item in the database //
module.exports.createSugar = (req, res) => {
  Sugar.create({
    sugar: req.body.sugar,
    lovibond: req.body.lovibond,
    potential: req.body.potential,
    description: req.body.description
  },function(err, sugar){
    if(err, sugar){
      sendJSONResponse(res, 400, err);
    }else{
      sendJSONResponse(res, 200, sugar);
    }
  });
};

// Get the name and description of all of the malts from the database //
module.exports.sugars = (req, res) => {
  Sugar
    .find()
    //.select('sugar description')
    .exec(function(err, sugar){
      sendJSONResponse(res, 200, sugar);
    })
};

// Get malt from data by by the ID number //
module.exports.sugarByID = (req, res) => {
  Sugar
    .findById(req.params.sugarid)
    .exec(function(err, sugar){
      sendJSONResponse(res, 200, sugar);
    })
};

// Update the Sugar information by the ID number //
module.exports.updateSugar = (req, res) => {};


// Delete the Sugar from the database by the ID nmber //
module.exports.deleteSugar = (req, res) => {
  var sugarid = req.params.sugarid;
  if(sugarid){
    Sugar
      .findByIdAndRemove(sugarid)
      .exec(function(err, sugar){
        if(err){
          sendJSONResponse(res, 404, err);
          return;
        }
        sendJSONResponse(res, 204, null);
      })
  } else {
    sendJSONResponse(res, 404,{
      "message": "No Sugar ID found"
    });
  }
};

var sendJSONResponse = (res, status, content) => {
  res.status(status);
  res.json(content);
};
