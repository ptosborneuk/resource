var mongoose = require('mongoose');

var hopSchema = new mongoose.Schema({
  name: { type: String, required: true },
  alpha: String,
  style: String,
  substitute: String,
  flavour: String
});

mongoose.model('hop', hopSchema, 'hops');
