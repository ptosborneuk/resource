var mongoose = require('mongoose');
var gracefulShutdown;
var DBURL = 'mongodb://localhost/brewing';

mongoose.set('useUnifiedTopology', true);
mongoose.connect(DBURL, { useNewUrlParser: true });

mongoose.connection.on('connected', function(){
  console.log('Mongoose connected to ' + DBURL );
});
mongoose.connection.on('error', function(err){
  console.log('Mongoose connetion error : ' + err);
});
mongoose.connection.on('disconnected', function(){
  console.log('Mongoose disconnected');
});

gracefulShutdown = function(msg, callback){
  mongoose.connection.close(function(){
    console.log('Mongoose disconnected through ' + msg);
    callback();
  });
};

// For nodemon restarts //
process.once('SIGUSR2', function(){
  gracefulShutdown('nodemon restart', function(){
    process.kill(process.pid, 'SIGUSR2');
  });
});
// For app terminating //
process.on('SIGINT', function(){
  gracefulShutdown('app termination', function(){
    process.exit(0);
  });
});
// For heroku app termination //
process.on('SIGTERM', function(){
  gracefulShutdown('Heroku app shutdown', function(){
    process.exit(0);
  });
});

require('./malt');
require('./style');
require('./hops');
require('./sugar');
require('./yeast');
