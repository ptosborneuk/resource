var mongoose = require('mongoose');

var sugarSchema = new mongoose.Schema({
  sugar: { type: String, required: true },
  lovibond: String,
  potential: String,
  description: String
});

mongoose.model('sugar', sugarSchema, 'sugars');
