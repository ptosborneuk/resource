var mongoose = require('mongoose');

var maltSchema = new mongoose.Schema({
  name: { type: String, required: true },
  type: String ,
  lovibond: String,
  srm: String ,
  ebc: String ,
  potential: String ,
  description: String
});

mongoose.model('malt', maltSchema, 'malts');
