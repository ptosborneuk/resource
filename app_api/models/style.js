var mongoose = require('mongoose');

var styleSchema = new mongoose.Schema({
  style: { type: String, required: true },
  ibu: String,
  srm: String,
  original: String,
  final: String,
  abv: String,
  carbination: String,
  impression: String,
  aroma: String,
  appearance: String,
  flavour: String,
  mouthfeel: String,
  comments: String,
  history: String,
  ingredients: String,
  comparison: String
});

mongoose.model('style', styleSchema, 'styles');
