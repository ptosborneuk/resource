var mongoose = require('mongoose');

var yeastSchema = new mongoose.Schema({
  code: { type: String, required: true },
  strain: String,
  make: String,
  brewery: String
});

mongoose.model('yeast', yeastSchema, 'yeasts');
