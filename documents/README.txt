 To install MongoDB

 - Download latest version from https://www.mongodb.com/ and install using instructions from website

 - Create a database in the MongoDB called brewing
 - Creating collections and import data from cvs files
     - Create the following collections hops, malts, styles, sugars and yeasts
     - select the collection and then the Import File option from the Add Data button
     - In the pop-up form slect the CSV option and then browse to the relevant data file in the /documents/ folder
     - Select import


To Run the Web App

- Download and install Node.js - https://nodejs.org/en/
- Down load files from the bitbucket to local drive
- Open a command line and Navigate to the folder containing the web App
- install nodemon using the command - npm install -g nodemon
- Run the web app using the command - nodemon
- Navigate to http://localhost:3000/


To see the raw json response from the web api's go to the following urls
Hops - http://localhost:3000/api/hops
Malts - http://localhost:3000/api/malts
Styles - http://localhost:3000/api/styles
Sugars - http://localhost:3000/api/sugars
Yeasts - http://localhost:3000/api/yeast/strains
