var express = require('express');
var router = express.Router();

var maltCtrl = require('../controllers/malts');
var hopCtrl = require('../controllers/hops');
var sugarCtrl = require('../controllers/sugars');
var yeastCtrl = require('../controllers/yeasts');
var styleCtrl = require('../controllers/styles');

router.get('/malts', maltCtrl.malts);
router.get('/malt/:maltid', maltCtrl.malt);
router.get('/malts/new', maltCtrl.newMalt);
router.post('/malts/new', maltCtrl.doAddMalt);
// router.delete('/malt/:maltid', maltCtrl.deleteMalt);

router.get('/hops', hopCtrl.hops);
router.get('/hop/:hopid', hopCtrl.hop);
router.get('/hops/new', hopCtrl.newHop);
router.post('/hops/new', hopCtrl.doAddHop);
// router.delete('/hop/:hopid', hopCtrl.deleteHop);

router.get('/sugars', sugarCtrl.sugars);
router.get('/sugar/:sugarid', sugarCtrl.sugar);
router.get('/sugars/new', sugarCtrl.newSugar);
router.post('/sugars/new', sugarCtrl.doAddSugar);
// router.delete('/sugar/:sugarid', sugarCtrl.deletesugar);

router.get('/strains', yeastCtrl.strains);
router.get('/yeasts/:strainid', yeastCtrl.yeasts)

router.get('/styles', styleCtrl.styles);
router.get('/style/:styleid', styleCtrl.style);

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

module.exports = router;
