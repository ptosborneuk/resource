var request = require('request');
const apiUtil = require('./utils');

module.exports.styles = (req, res) => {
  var requestOptions, path;
  path = '/api/styles';
  requestOptions = {
    url : apiUtil.apiOptions.server + path,
    method : "GET",
    json : {},
    qs : {}
  };
  request(
    requestOptions,
    function(err, response, body){
      renderStyles(req, res, body)
    }
  )
};

/* Get single malt infromation */
module.exports.style = (req, res) => {
  var requestOptions, path;
  path = "/api/style/" + req.params.styleid;
  console.log(path);
  requestOptions = {
    url : apiUtil.apiOptions.server + path,
    method : "GET",
    json : {},
    qs : {}
  };
  request(
    requestOptions,
    function(err, response, body){
      renderStyle(req, res, body)
    }
  )
};

var renderStyles = (req, res, responseBody) => {
  res.render('styles', {
    title: 'List of Beer Styles',
    styles: responseBody
  });
};

var renderStyle = (req, res, responseBody) => {
  res.render('style', {
    title: 'Information about a beer style',
    style: responseBody
  });
};
