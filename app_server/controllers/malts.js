var request = require('request');
const apiUtil = require('./utils');

/*
  Retrieve a list of all of the malts from the web api
  pass the results to the renderMalts operation using the body parameter
*/
module.exports.malts = (req, res) => {
  var requestOptions, path;
  path = '/api/malts';
  requestOptions = {
    url : apiUtil.apiOptions.server + path,
    method : "GET",
    json : {},
    qs : {}
  };
  request(
    requestOptions,
    function(err, response, body){
      renderMalts(req, res, body)
    }
  )
};

/*
  Retrieve a single malt item from the web api using its _id number
  pass the result to the renderMalt operation using the body parameter
*/
module.exports.malt = (req, res) => {
  var requestOptions, path;
  path = "/api/malt/" + req.params.maltid;
  requestOptions = {
    url : apiUtil.apiOptions.server + path,
    method : "GET",
    json : {},
    qs : {}
  };
  request(
    requestOptions,
    function(err, response, body){
      renderMalt(req, res, body)
    }
  )
};

/* Render the newMalt form */
module.exports.newMalt = (req, res) => {
  renderMaltForm(req, res);
};

/*
  Creat a postdata object using the informtion for the newMalt Form
  Post the postdata object to the create new malt web api
  redirect the the malts page
*/
module.exports.doAddMalt = (req, res, responseBody) => {
  var requestOptions, path;
  path = "/api/malts";
  postdata = {
    name: req.body.name,
    type: req.body.type,
    lovibond: req.body.lovibond,
    srm: req.body.srm,
    ebc: req.body.ebc,
    potential: req.body.potential,
    description: req.body.description
  };
  requestOptions = {
    url : apiUtil.apiOptions.server + path,
    method : "POST",
    json : postdata
  };
  request(
    requestOptions,
      function(err, response, body){
        if (response.statusCode === 201){
          console.log(response.statusCode);
          res.redirect('/malts');
        }else{
          console.log(response.statusCode);
          res.redirect('/malts');
        }
      }
  );
};

/*
  Displays a list of all the malts using the information contained in the responseBody object
*/
var renderMalts = (req, res, responseBody) => {
  res.render('malts', {
    title: 'List of Malts',
    malts: responseBody
  });
};

/*
  Displays the information about a single malt using the information contained in the responseBody object
*/
var renderMalt = (req, res, responseBody) => {
  res.render('malt', {
    title: 'Malt Information',
    malt: responseBody
  });
};

/*
  Displays a blank new malt form
*/
var renderMaltForm = (req, res) => {
  res.render('malt-form', {
    title: 'Create a new Malt',
    pageHeader: { title: 'New Malt Form' }
  });
}
