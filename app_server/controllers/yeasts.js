var request = require('request');
const apiUtil = require('./utils');

module.exports.strains = (req, res) => {
  var requestOptions, path;
  path = '/api/yeast/strains';
  requestOptions = {
    url : apiUtil.apiOptions.server + path,
    method : "GET",
    json : {},
    qs : {}
  };
  request(
    requestOptions,
    function(err, response, body){
      renderStrains(req, res, body)
    }
  )
};

module.exports.yeasts = (req, res) => {
  var requestOptions, path;
  path = '/api/yeasts/'  + req.params.strainid;
  requestOptions = {
    url : apiUtil.apiOptions.server + path,
    method : "GET",
    json : {},
    qs : {}
  };
  request(
    requestOptions,
    function(err, response, body){
      renderYeasts(req, res, body)
    }
  )
};

/* Get single malt infromation */
module.exports.yeast = (req, res) => {
  var requestOptions, path;
  path = "/api/yeast/" + req.params.yeastid;
  console.log(path);
  requestOptions = {
    url : apiUtil.apiOptions.server + path,
    method : "GET",
    json : {},
    qs : {}
  };
  request(
    requestOptions,
    function(err, response, body){
      renderYeast(req, res, body)
    }
  )
};

var renderStrains = (req, res, responseBody) => {
  res.render('strains', {
    title: 'List of Yeast Strains',
    strains: responseBody
  });
};

var renderYeasts = (req, res, responseBody) => {
  res.render('yeasts', {
    title: 'List of yeasts',
    yeasts: responseBody
  });
};

var renderYeast = (req, res, responseBody) => {
  res.render('yeast', {
    title: 'Yeast Information',
    yeast: responseBody
  });
};
