var request = require('request');
const apiUtil = require('./utils');

/*
  Retrieve a list of all of the sugars from the web api
  pass the results to the renderSugars operation using the body parameter
*/
module.exports.sugars = (req, res) => {
  var requestOptions, path;
  path = '/api/sugars';
  requestOptions = {
    url : apiUtil.apiOptions.server + path,
    method : "GET",
    json : {},
    qs : {}
  };
  request(
    requestOptions,
    function(err, response, body){
      renderSugars(req, res, body)
    }
  )
};

/*
  Retrieve a single sugar item from the web api using its _id number
  pass the result to the renderSugar operation using the body parameter
*/
module.exports.sugar = (req, res) => {
  var requestOptions, path;
  path = "/api/sugar/" + req.params.sugarid;
  console.log(path);
  requestOptions = {
    url : apiUtil.apiOptions.server + path,
    method : "GET",
    json : {},
    qs : {}
  };
  request(
    requestOptions,
    function(err, response, body){
      renderSugar(req, res, body)
    }
  )
};

/* Render the newSugar form */
module.exports.newSugar = (req, res) => {
  renderSugarForm(req, res);
};

/*
  Creat a postdata object using the informtion from the newSugarForm Form
  Post the postdata object to the create new sugar web api
  redirect the the sugars page
*/
module.exports.doAddSugar = (req, res, responseBody) => {
  var requestOptions, path;
  path = "/api/sugars";
  postdata = {
    sugar: req.body.sugar,
    lovibond: req.body.lovibond,
    potential: req.body.potential,
    description: req.body.description
  };
  requestOptions = {
    url : apiUtil.apiOptions.server + path,
    method : "POST",
    json : postdata
  };
  request(
    requestOptions,
      function(err, response, body){
        if (response.statusCode === 201){
          console.log(response.statusCode);
          res.redirect('/sugars');
        }else{
          console.log(response.statusCode);
          res.redirect('/sugars');
        }
      }
  );
};

/*
  Displays a list of all the sugars using the information contained in the responseBody object
*/
var renderSugars = (req, res, responseBody) => {
  res.render('sugars', {
    title: 'List of sugars used in the brewing process',
    sugars: responseBody
  });
};

/*
  Displays the information about a single sugar using the information contained in the responseBody object
*/
var renderSugar = (req, res, responseBody) => {
  res.render('sugar', {
    title: 'Information about a sugar',
    sugar: responseBody
  });
};

/*
  Displays a blank new sugar form
*/
var renderSugarForm = (req, res) => {
  res.render('sugar-form', {
    title: 'Create a new Sugar',
    pageHeader: { title: 'New Sugar Form' }
  });
}
