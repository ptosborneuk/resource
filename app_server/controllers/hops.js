var request = require('request');
const apiUtil = require('./utils');

/*
  Retrieve a list of all of the hops from the web api
  pass the results to the renderMalts operation using the body parameter
*/
module.exports.hops = (req, res) => {
  var requestOptions, path;
  path = '/api/hops';
  requestOptions = {
    url : apiUtil.apiOptions.server + path,
    method : "GET",
    json : {},
    qs : {}
  };
  request(
    requestOptions,
    function(err, response, body){
      renderHops(req, res, body)
    }
  )
};

/*
  Retrieve a single hop item from the web api using its _id number
  pass the result to the renderHop operation using the body parameter
*/
module.exports.hop = (req, res) => {
  var requestOptions, path;
  path = "/api/hop/" + req.params.hopid;
  console.log(path);
  requestOptions = {
    url : apiUtil.apiOptions.server + path,
    method : "GET",
    json : {},
    qs : {}
  };
  request(
    requestOptions,
    function(err, response, body){
      renderHop(req, res, body)
    }
  )
};

/* Render the newHop form */
module.exports.newHop = (req, res) => {
  renderHopForm(req, res);
};

/*
  Creat a postdata object using the informtion for the newHop Form
  Post the postdata object to the create new hop web api
  redirect the the hops page
*/
module.exports.doAddHop = (req, res, responseBody) => {
  var requestOptions, path;
  path = "/api/hops";
  postdata = {
    name: req.body.name,
    alpha: req.body.alpha,
    style: req.body.style,
    substitute: req.body.substitute,
    flavour: req.body.flavour
  };
  requestOptions = {
    url : apiUtil.apiOptions.server + path,
    method : "POST",
    json : postdata
  };
  request(
    requestOptions,
      function(err, response, body){
        if (response.statusCode === 201){
          console.log(response.statusCode);
          res.redirect('/hops');
        }else{
          console.log(response.statusCode);
          res.redirect('/hops');
        }
      }
  );
};

/*
  Displays a list of all the hops using the information contained in the responseBody object
*/
var renderHops = (req, res, responseBody) => {
  res.render('hops', {
    title: 'List of Hops',
    hops: responseBody
  });
};

/*
  Displays the information about a single hop using the information contained in the responseBody object
*/
var renderHop = (req, res, responseBody) => {
  res.render('hop', {
    title: 'Hop Information',
    hop: responseBody
  });
};

/*
  Displays a blank new hop form
*/
var renderHopForm = (req, res) => {
  res.render('hop-form', {
    title: 'Create a new hop',
    pageHeader: { title: 'New Hop Form' }
  });
}
